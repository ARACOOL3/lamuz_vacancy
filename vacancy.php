<?php
require __DIR__ . '/vendor/autoload.php';

use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator as V;

$tmpDir = __DIR__ . '/tmp';
if (!is_dir($tmpDir)) {
    mkdir($tmpDir, 0775);
}

// Upload cv file
if ((!empty($_FILES['cv'])) && ($_FILES['cv']['error'] == 0)) {
    $filename = basename($_FILES['cv']['name']);
    $ext = substr($filename, strrpos($filename, '.') + 1);
    if ($ext != 'exe' || $ext != 'bat' || $_FILES["cv"]["size"] <= 1048576) {
        $newName = $tmpDir . DIRECTORY_SEPARATOR . strtolower(strip_tags($filename));
        if (!move_uploaded_file($_FILES['cv']['tmp_name'], $newName)) {
            echo 'CV files not uploaded';
            exit;
        }
    }
}

$fioValidator = V::notBlank()->alnum()->setName('fio');
$jobValidator = V::notBlank()->alnum()->setName('job');
$eduValidator = V::notBlank()->alnum()->setName('edu');
$cvValidator = V::notBlank()->alnum()->setName('cv_text');
$emailValidator = V::notBlank()->email()->setName('email');
$phoneValidator = V::notBlank()->phone()->setName('phone');

$userRequest = (object)$_POST;
try {
    $fioValidator->assert($userRequest->fio);
    $jobValidator->assert($userRequest->job);
    $eduValidator->assert($userRequest->edu);
    $cvValidator->assert($userRequest->cv);
    $emailValidator->assert($userRequest->email);
    $phoneValidator->assert($userRequest->phone);
} catch (NestedValidationException $exception) {
    echo $exception->getFullMessage();
    exit;
}

$mail = new Message;
$mail->setFrom($userRequest->fio . ' ' . $userRequest->email)
    ->addTo('khasanov.b@gmail.com')
    ->setSubject('Lamuz vacancy')
    ->setBody(implode("\n\n", [
        $userRequest->fio,
        $userRequest->job,
        $userRequest->edu,
        $userRequest->email,
        $userRequest->phone,
        $userRequest->cv
    ]));

if (isset($newName)) {
    $mail->addAttachment($newName);
    unlink($newName);
}

$mailer = new SendmailMailer;
$mailer->send($mail);